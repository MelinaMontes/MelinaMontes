
![Melina Montes](/images/githublogo.png)

<h1 align="center">Hello 👋</h1>
<h3 align="center">I'm a Jr Backend Developer</h3>

- 🔭 I’m currently working on  **Api Rest**

- 🌱 I’m currently learning **Java, Spring Boot, Maven**

- 👯 I’m looking to collaborate on **any kind of Backend projects**

- 🤝 I’m looking for help with **Docker**

- 👨‍💻 All of my projects are available at [https://github.com/MelinaMontes](https://github.com/MelinaMontes), [https://hub.docker.com/u/mdp9mm86cat08](https://hub.docker.com/u/mdp9mm86cat08)

- 📫 How to reach me **mell.baun@gmail.com**

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://dev.to/https://dev.to/melinamontes" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/dev-dot-to.svg" alt="https://dev.to/melinamontes" height="30" width="40" /></a>
</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://www.gnu.org/software/bash/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.java.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/> </a> <a href="https://www.linux.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> <a href="https://www.mysql.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a> <a href="https://postman.com" target="_blank"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/> </a> <a href="https://spring.io/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring" width="40" height="40"/> </a> </p>
